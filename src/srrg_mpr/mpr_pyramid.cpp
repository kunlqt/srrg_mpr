#include "mpr_pyramid.h"
#include "mpr_utils.h"

namespace srrg_mpr {
using srrg_core::Float3Image;
using srrg_core::RawDepthImage;
using srrg_core::Float5Image;
using srrg_core::FloatImage;
using srrg_core::IntImage;
using srrg_core::UnsignedCharImage;
using srrg_core::Cloud3D;
using srrg_core::Vec5f;

MPRPyramidLevel::MPRPyramidLevel(int rows_, int cols_) {
  resize(rows_, cols_);
  _min_depth = 0;
  _max_depth = 0;

  _sensor_offset = Eigen::Isometry3f::Identity();
}

void MPRPyramidLevel::resize(int rows_, int cols_) {
  _rows = rows_;
  _cols = cols_;
  if (_rows && _cols) {
    _mask.create(_rows, _cols);
    _image.resize(_rows, _cols);
  }
}

void MPRPyramidLevel::initialize(const FloatImage& intensity,
                                 const FloatImage& depth,
                                 const Float3Image& normals) {
  cv::Size s(_cols, _rows);
  if (intensity.size() != s || depth.size() != s || normals.size() != s) {
    throw std::runtime_error("image size mismatch");
  }
  for (int r = 0; r < _rows; ++r) {
    const float* i_ptr = intensity.ptr<const float>(r);
    const float* d_ptr = depth.ptr<const float>(r);
    const cv::Vec3f* n_ptr = normals.ptr<const cv::Vec3f>(r);
    MPRPyramidImageEntry* dest_ptr = _image.rowPtr(r);
    for (int c = 0; c < _cols; ++c, ++i_ptr, ++d_ptr, ++n_ptr, ++dest_ptr) {
      const cv::Vec3f& n = *n_ptr;
      MPRPyramidImageEntry& dest = *dest_ptr;
      dest._point << *i_ptr, *d_ptr, n[0], n[1], n[2];
    }
  }
}

cv::Mat MPRPyramidLevel::getChannel(MPRPyramidLevel::ChannelType type,
                                    MPRPyramidLevel::ChannelMode mode) const {
  cv::Mat dest;
  int cv_type = 0;
  int offset = 0;
  int count = 1;

  switch (type) {
    case Intensity:
      cv_type = CV_32FC1;
      offset = 0;
      count = 1;
      break;
    case Depth:
      cv_type = CV_32FC1;
      offset = 1;
      count = 1;
      break;
    case Normals:
      cv_type = CV_32FC3;
      count = 3;
      offset = 2;
      break;
  }
  dest.create(_rows, _cols, cv_type);
  for (int r = 0; r < _rows; ++r) {
    const MPRPyramidImageEntry* entry_ptr = _image.rowPtr(r);
    float* dest_ptr = dest.ptr<float>(r);
    for (int c = 0; c < _cols; ++c, ++entry_ptr) {
      const MPRPyramidImageEntry& entry = *entry_ptr;
      Vector5f src;
      switch (mode) {
        case Plain: src = entry._point; break;
        case RowDerivative: src = entry._derivatives.col(1); break;
        case ColumnDerivative: src = entry._derivatives.col(0); break;
      }
      for (int k = 0; k < count; ++k, ++dest_ptr) *dest_ptr = src[offset + k];
    }
  }
  return dest;
}

void MPRPyramidLevel::scale(MPRPyramidLevel& dest) const {
  if (_rows % dest._rows) {
    throw std::runtime_error(
        "MPRPyramidLevel::scale: the rows of dest should divide perfectly the "
        "rows of this");
  }
  if (_cols % dest._cols) {
    throw std::runtime_error(
        "MPRPyramidLevel::scale: the rows of dest should divide perfectly the "
        "rows of this");
  }

  int row_skip = _rows / dest._rows;
  int col_skip = _cols / dest._cols;
  int src_r = 0;
  for (int r = 0; r < dest._rows; ++r, src_r += row_skip) {
    const MPRPyramidImageEntry* src_ptr = _image.rowPtr(src_r);
    MPRPyramidImageEntry* dest_ptr = dest._image.rowPtr(r);

    const unsigned char* src_mask_ptr = _mask.ptr<const unsigned char>(src_r);
    unsigned char* dest_mask_ptr = dest._mask.ptr<unsigned char>(r);

    for (int c = 0; c < dest._cols; ++c, ++dest_ptr, ++dest_mask_ptr,
             src_ptr += col_skip, src_mask_ptr += col_skip) {
      dest_ptr->_point = src_ptr->_point;
      *dest_mask_ptr = *src_mask_ptr;
    }
  }
}

cv::Mat MPRPyramidLevel::getTiledChannels() const {
  std::vector<cv::Mat> pyr_tiles(
      3);  // these are the row stripes of the final image

  // no offset no scaling on intensity
  // no offset, scaling at 0.3 on depth
  // default for normals

  // original
  std::vector<cv::Mat> stripe(3);
  stripe[0] = replicateChannels(
      getChannel(MPRPyramidLevel::Intensity, MPRPyramidLevel::Plain));

  stripe[1] = replicateChannels(
      getChannel(MPRPyramidLevel::Depth, MPRPyramidLevel::Plain));
  stripe[1] *= (3. / _max_depth);

  // The normals are in [-1, 1], so we abs them to make them show nicely.
  stripe[2] = getChannel(MPRPyramidLevel::Normals, MPRPyramidLevel::Plain);
  stripe[2] = cv::abs(stripe[2]);
  tileImages(pyr_tiles[0], stripe, true);

  // rows
  stripe[0] = replicateChannels(
      getChannel(MPRPyramidLevel::Intensity, MPRPyramidLevel::RowDerivative));

  stripe[1] = replicateChannels(
      getChannel(MPRPyramidLevel::Depth, MPRPyramidLevel::RowDerivative));
  stripe[1] *= 0.3;

  stripe[2] =
      getChannel(MPRPyramidLevel::Normals, MPRPyramidLevel::RowDerivative);
  stripe[2] *= 0.5;
  stripe[2] += cv::Vec3f(0.5, 0.5, 0.5);
  tileImages(pyr_tiles[1], stripe, true);

  // cols
  stripe[0] = replicateChannels(getChannel(MPRPyramidLevel::Intensity,
                                           MPRPyramidLevel::ColumnDerivative));

  stripe[1] = replicateChannels(
      getChannel(MPRPyramidLevel::Depth, MPRPyramidLevel::ColumnDerivative));
  stripe[1] *= 0.3;

  stripe[2] =
      getChannel(MPRPyramidLevel::Normals, MPRPyramidLevel::ColumnDerivative);
  stripe[2] *= 0.5;
  stripe[2] += cv::Vec3f(0.5, 0.5, 0.5);
  tileImages(pyr_tiles[2], stripe, true);
  cv::Mat total_pyramid_image;

  tileImages(total_pyramid_image, pyr_tiles, false);
  return total_pyramid_image;
}
}
