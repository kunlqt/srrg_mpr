#pragma once
#include "mpr_pyramid.h"
#include "srrg_types/vector_2d.h"
#include <iostream>
#include <limits>

namespace srrg_mpr {

using srrg_core::RichPoint3D;
using srrg_core::Matrix6f;
using srrg_core::Vector6f;
using srrg_core::Vec5f;
typedef Eigen::Matrix<float, 5, 1> Vector5f;
typedef Eigen::Matrix<float, 2, 6> Matrix2_6f;
typedef Eigen::Matrix<float, 5, 6> Matrix5_6f;
typedef Eigen::Matrix<float, 5, 2> Matrix5_2f;

// MPRSolver: this class implements a least squares solver
//            for Photometric error minimization.
class MPRSolver {
  using Cloud = srrg_core::Cloud3D;
  struct WorkspaceEntry;

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  enum PointStatusFlag {
    Good = 0x00,
    Outside = 0x1,
    DepthOutOfRange = 0x2,
    Masked = 0x3,
    Occluded = 0x4,
    DepthError = 0x5,
    Invalid = 0x7
  };

  struct Config {
    float damping = 1.;
    float omega_intensity = 1.;
    float omega_depth = 1.;
    float omega_normals = 1.;
    float depth_error_rejection_threshold = 0.25;
    float kernel_chi_threshold = 1.;
    bool verbose = false;
  };

  static const int _num_point_states = 8;

  struct PointStatus {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Vector5f _error;
    float _chi;
    PointStatusFlag _flag;
  };

  typedef std::vector<PointStatus, Eigen::aligned_allocator<PointStatus> >
      PointStatusVector;

  MPRSolver();
  ~MPRSolver() {}

  // set methods
  void setPyramidLevel(const MPRPyramidLevel& pyramid_level);
  void setCloud(const Cloud& cloud);
  inline void setT(const Eigen::Isometry3f T_ = Eigen::Isometry3f::Identity()) {
    _T = T_;
  }

  // get methods
  inline Eigen::Isometry3f T() { return _T; }
  inline float totalChi() const { return _total_chi; }
  inline int numInliers() const { return _num_inliers; }
  inline double timeLinearize() const { return _time_linearize; }
  inline double timeProjections() const { return _time_projections; }
  const int* measurementStatesStats() const { return _point_states_stats; }
  inline const Config& constConfig() const { return _config; }
  inline Config& mutableConfig() { return _config; }
  const PointStatusVector& pointStates() const { return _point_states; }
  cv::Mat getErrorImage(MPRPyramidLevel::ChannelType type) const;
  cv::Mat getTiledErrorImage() const;

  void compute();

 protected:
  // compute the pieces needed for the minimization
  void linearize();
  // error and Jacobian computation
  PointStatusFlag errorAndJacobian(Vector5f& error,
                                   Matrix5_6f& jacobian,
                                   const WorkspaceEntry& entry);
  // this one performs the occlusion check
  void computeProjections();
  // update convergence/timing stats
  void updateStats();
  // check Solver state before the compute call
  void checkCompute();

 private:
  Config _config;  // the Configuration

  const MPRPyramidLevel* _level_ptr;  // input Pyramid Level
  const srrg_core::Cloud3D* _cloud;   // input Cloud

  Eigen::Isometry3f _T;  // output Transform

  int _rows;         // Pyramid Level rows
  int _cols;         // Pyramid Level cols
  float _min_depth;  // Pyramid Level min depth
  float _max_depth;  // Pyramid Level max depth
  Eigen::Matrix3f
      _sensor_offset_rotation_inverse;  // Pyramid Level sensor rotation inverse
  Eigen::Isometry3f
      _sensor_offset_inverse;      // Pyramid Level sensor offset inverse
  Eigen::Matrix3f _camera_matrix;  // Pyramid Level camera matrix
  MPRPyramidLevel::CameraType _camera_type;  // Pyramid Level camera type

  float _omega_intensity_sqrt;  // Config omega intensity
  float _omega_depth_sqrt;      // Config omega depth
  float _omega_normals_sqrt;    // Config omega normals

  srrg_core::Matrix3_6f _J_icp;  // cached quantity to speed up J computation

  Matrix6f _H;  // 'Hessian' matrix for linear solution
  Vector6f _b;  // residual vector for linear solution

  double _time_linearize;                      // Stats: linearizing time
  double _time_projections;                    // Stats: projection time
  int _point_states_stats[_num_point_states];  // Stats: number of points
  float _total_chi;                            // Stats: total chi
  int _num_inliers;                            // Stats: number of inliers

  PointStatusVector _point_states;  // Stats: stat for each point of the cloud

  // WorkspaceEntry: an entry of the Solver is represented in this way
  struct WorkspaceEntry {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    WorkspaceEntry(
        const int index = -1,
        const float intensity = 0.f,
        const float depth = std::numeric_limits<float>::max(),
        const Eigen::Vector3f& point = Eigen::Vector3f::Zero(),
        const Eigen::Vector3f& normal = Eigen::Vector3f::Zero(),
        const Eigen::Vector3f& camera_point = Eigen::Vector3f::Zero(),
        const Eigen::Vector2f& image_point = Eigen::Vector2f::Zero()) {
      _index = index;
      _depth = depth;
      _intensity = intensity;
      _point = point;
      _normal = normal;
      _camera_point = camera_point;
      _image_point = image_point;
    }

    // get the prediction for the solver as a Vector5f
    Vector5f prediction() const {
      Vector5f pred;
      pred << _intensity, _depth, _normal[0], _normal[1], _normal[2];
      return pred;
    }

    int _index;                     // index of projected point
    float _intensity;               // intensity
    float _depth;                   // depth
    Eigen::Vector3f _point;         // point after applying transform
    Eigen::Vector3f _normal;        // normal after applying transform
    Eigen::Vector3f _camera_point;  // camera point in camera coords if pinhole,
                                    // in polar if spherical
    Eigen::Vector2f _image_point;   // point in image coords
  };

  typedef srrg_core::Vector2D<WorkspaceEntry,
                              Eigen::aligned_allocator<WorkspaceEntry> >
      Workspace;

  Workspace _workspace;
};
}
